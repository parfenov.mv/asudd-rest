package ru.vms.asudd.rest.route;

import javax.ws.rs.*;


@Path("/api/")
public class ImageService {
    
    @GET
    @Path("/test/")
    @Produces("application/xml")
    public String test() {
        return null;
    }

    @GET
    @Path("/images/{id}/")
    @Produces("application/xml")
    public String getImageList(@PathParam("id") String id, @QueryParam("dateFrom")String dateFrom, @QueryParam("dateTo")String dateTo) {
        return null;
    }
}

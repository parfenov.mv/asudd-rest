package ru.vms.asudd.rest.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.simple.JSONArray;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;



public class ServiceHandler {
	
 	private String ftpServer; // = "192.168.100.146";
    private int ftpPort; // = 21;
    private String user; // = "Photo";
    private String pass; // = "6YHN$fg";
    private DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

    public void init(){
    	Properties prop = new Properties();
    	InputStream input = null;

    	try {
    		input = new FileInputStream("./etc/asudd.photo.config.properties");
    		prop.load(input);

    		// get the property value and print it out
    		ftpServer = prop.getProperty("ftp_server");
    		ftpPort = Integer.parseInt(prop.getProperty("ftp_port"));
    		user = prop.getProperty("ftp_user");
    		pass = prop.getProperty("ftp_password");

    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		if (input != null) {
    			try {
    				input.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    
    }
    
    public String test() {
    	return "{\"test\": \"test\"}";
    }
    
    private int StrToIntDef(String value, Integer defValue) {
		int i = defValue;
    	try {
    		i = Integer.parseInt(value);
    	} catch (Exception e) {}
    	return i;
	}
    
	private Date getFileDate(String fileName) {
		Date result;
		String partFileName = fileName.split("__")[1].split("\\.")[0];
		try {
			result = format.parse(partFileName);
		} catch (ParseException ex) {
			// если конвертнуть имя файла в дату не получилось, вернем столетнюю дату, 
			// чтобы обрабатываемый файл не пролез через фильтр.
			Calendar cal = Calendar.getInstance();
			cal.setTime (new Date());
			int yearsToDecrement = -100;
			cal.add(Calendar.YEAR, yearsToDecrement);
			result = cal.getTime();
		}
		return result;
	}
	
    
  	private List<String> getFTPFiles(String sensorId, Date dateFrom, Date dateTo) {
	    List<String> paths = new ArrayList<String>();
	    List<String> filePaths = new ArrayList<String>();
  		
  		DateFormat yearMonthFormat = new SimpleDateFormat("yyyyMM");
  		int yearMonthFrom = Integer.parseInt(yearMonthFormat.format(dateFrom));
  		int yearMonthTo = Integer.parseInt(yearMonthFormat.format(dateTo));
  		
  		DateFormat dayFormat = new SimpleDateFormat("dd");
  		int dayFrom = Integer.parseInt(dayFormat.format(dateFrom));
  		int dayTo = Integer.parseInt(dayFormat.format(dateTo));
  		
  		DateFormat fullDateFormat = new SimpleDateFormat("yyyyMMdd");
  		int fullDateFrom = Integer.parseInt(fullDateFormat.format(dateFrom));
  		int fullDateTo = Integer.parseInt(fullDateFormat.format(dateTo));
  		
  		FTPClient ftpClient = new FTPClient();
  	        
  	    try {
  	    	ftpClient.setConnectTimeout(5000); 
  	    	ftpClient.connect(ftpServer, ftpPort);
  	 
  	        int replyCode = ftpClient.getReplyCode();
  	        if (!FTPReply.isPositiveCompletion(replyCode)) {
  	            return filePaths;
  	        }
  	 
  	        boolean success = ftpClient.login(user, pass);
  	 
  	        if (!success) {
  	            return filePaths;
  	        }
  	 
  	        String[] list;
  	        list = ftpClient.listNames();
  	        List<String> folders = Arrays.stream(list)
  	        		.filter(item -> (StrToIntDef(item.replace("-", ""), 0) >= yearMonthFrom && StrToIntDef(item.replace("-", ""), 0) <= yearMonthTo))
  	        		.collect(Collectors.toList());
  	        
  	        
  	        for (String folder: folders) {
  	        	System.out.println(folder);
  	        	ftpClient.changeWorkingDirectory(folder);
  	        	list = ftpClient.listNames();
  	        	
  	        	
  	        	if (yearMonthFrom == yearMonthTo) {
  	        		// если yearMonthFrom == yearMonthTo значит запрошенны данные внутри одного месяца
  		        	Arrays.stream(list)
  		        		.filter(item -> (StrToIntDef(item, 0) >= dayFrom && StrToIntDef(item, 0) <= dayTo))
  		        		.forEach(item -> paths.add("/" + folder + "/" + item));
  	        	} else {
  	        		// если yearMonthFrom != yearMonthTo значит запрошенны данные из разных месяцев
  	        		if (StrToIntDef(folder.replace("-", ""), 0) == yearMonthFrom) {
  	        			//если обрабатываем начальный месяц
  	        			Arrays.stream(list)
  		        			.filter(item -> (StrToIntDef(item, 0) >= dayFrom))
  		        			.forEach(item -> paths.add("/" + folder + "/" + item));
  	        		}
  	        		
  	        		if ((StrToIntDef(folder.replace("-", ""), 0) > yearMonthFrom) && (StrToIntDef(folder.replace("-", ""), 0) < yearMonthTo)) {
  	        			//если обрабатываем промежуточный месяц, то возвращаем все папки
  	        			Arrays.stream(list)
  	        				.forEach(item -> paths.add("/" + folder + "/" + item));
  	        		}
  	        			
  	        		
  	        		if (StrToIntDef(folder.replace("-", ""), 0) == yearMonthTo) {
  	        			//если обратываем конечный месяц
  	        			Arrays.stream(list)
  		        			.filter(item -> (StrToIntDef(item, 0) <= dayTo))
  		        			.forEach(item -> paths.add("/" + folder + "/" + item));
  	        		}
  	        	}
  	        	
  	        	//вернемся на уровень выше
  	        	ftpClient.changeToParentDirectory();
  	        }
  	        
  	        //теперь у нас есть набор из путей до папок, которые должны содержать запрашиваемые файлы
  	        //надо взять файлы из каждой из папок и отфильтровать файлы по идентификатору сенсора и времени
  	        for (String folder : paths) {
  	        	System.out.println(folder);
  	        	
  	        	ftpClient.changeWorkingDirectory(folder);
  	        	list = ftpClient.listNames();
  	        	// printNames(list);
  	        	
  	        	//фильтруем по идентификатору сенсораффф
  	        	Stream<String> files = Arrays.stream(list)
  	        		.filter(item -> (item.startsWith(sensorId)));
  	        	
  	        	if (fullDateFrom == fullDateTo) {
  	        		// если запрошенны данные из одной даты 
  	        		
  	        		files
  	        			.filter(file -> ((getFileDate(file).compareTo(dateFrom) >= 0) && (getFileDate(file).compareTo(dateTo) <= 0)))
  	        			.forEach(file -> filePaths.add(folder + "/" + file));

  	        	} else {
  	        		// если запрошенны данные из разных дат
  	        		
  	        		if (fullDateFrom == StrToIntDef(folder.replace("/", "").replace("-", ""), 0)) {
  	        			// если обрабатываем файлы из папки для первой даты	
  	        			files
  	        				.filter(file -> (getFileDate(file).compareTo(dateFrom) >= 0))
  	        				.forEach(file -> filePaths.add(folder + "/" + file));
  	        		}
  	        		
  	        		
  	        		if ((fullDateFrom < StrToIntDef(folder.replace("/", "").replace("-", ""), 0)) && (fullDateTo > StrToIntDef(folder.replace("/", "").replace("-", ""), 0))) {
  	        			files
          					.forEach(file -> filePaths.add(folder + "/" + file));
  	        		}
  	        		
  	        		
  	        		if (fullDateTo == StrToIntDef(folder.replace("/", "").replace("-", ""), 0)) {
  	        			// если обрабатываем файлы из папки для последней даты	
  	        			files
  	        				.filter(file -> (getFileDate(file).compareTo(dateTo) <= 0))
  	        				.forEach(file -> filePaths.add(folder + "/" + file));
  	        		}
  	        		
  	        	}
  	        }
  	        
  	 
  	 
  	        } catch (IOException ex) {  //| ParseException 
  	        	ex.printStackTrace();
  	        } finally {
  	            // logs out and disconnects from server
  	            try {
  	                if (ftpClient.isConnected()) {
  	                    ftpClient.logout();
  	                    ftpClient.disconnect();
  	                }
  	            } catch (IOException ex) {
  	            }
  	        }
  	    
  	    return filePaths;
  	}

    public String getImageList(String id, String dateFromStr, String dateToStr) {
		JSONArray responseObject = new JSONArray();
		try {			
			Date dateFrom = format.parse(dateFromStr);
		    Date dateTo = format.parse(dateToStr);
		    List<String> files = getFTPFiles(id, dateFrom, dateTo);
		    responseObject.addAll(files);     
		} catch (Exception e) {
	            e.printStackTrace();
	    }
		return responseObject.toJSONString();
    }
}
